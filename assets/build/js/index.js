var rangeSlider = document.getElementById("rs-range-line");
var rangeBullet = document.getElementById("rs-bullet");

rangeSlider.addEventListener("input", showSliderValue, false);

function showSliderValue() {
    rangeBullet.innerHTML = rangeSlider.value;
    var bulletPosition = (rangeSlider.value /rangeSlider.max);
    rangeBullet.style.left = (bulletPosition * 590) + "px";
}

var rangeSlider2 = document.getElementById("rs-range-line2");
var rangeBullet2 = document.getElementById("rs-bullet2");

rangeSlider2.addEventListener("input", showSliderValue2, false);

function showSliderValue2() {
    rangeBullet2.innerHTML = rangeSlider2.value;
    var bulletPosition2 = (rangeSlider2.value /rangeSlider2.max);
    rangeBullet2.style.left = (bulletPosition2 * 590) + "px";
}


// Pretty simple huh?



jQuery(document).ready(function () {
    var scene = jQuery('#scene').get(0);
    var parallaxInstance = new Parallax(scene);


    var scene2 = jQuery('#scene2').get(0);
    var parallaxInstance2 = new Parallax(scene2);

    var scene3 = jQuery('#scene3').get(0);
    var parallaxInstance3 = new Parallax(scene3);

    var scene4 = jQuery('#scene4').get(0);
    var parallaxInstance4 = new Parallax(scene4);

    jQuery('#fullpage').fullpage({
        anchors: ['firstPage', 'secondPage', 'thirdPage', 'fourthPage', 'fifthPage'],


    });

});



