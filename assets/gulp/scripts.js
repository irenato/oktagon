module.exports = function (gulp, plugins) {
    return function () {
        gulp.src('build/js/*.js')
            .pipe(plugins.terser())
            .pipe(gulp.dest('dist/js'));
    };
};