
module.exports = function (gulp, plugins) {
    return function () {
        gulp.src('build/scss/*.scss')
            .pipe(plugins.sass())
            .pipe(plugins.cleanCss())
            .pipe(gulp.dest('dist/css'));
    };
};