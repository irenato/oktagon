<?php

add_action( 'wp_enqueue_scripts', 'octagon_scripts' );

function octagon_scripts() {
    wp_enqueue_style( 'style', get_stylesheet_uri() );
    wp_enqueue_style( 'main-css', get_template_directory_uri() . '/assets/dist/css/style.css' );

    wp_enqueue_script( 'script-easings', get_template_directory_uri() . '/assets/dist/js/jquery.easings.min.js', array('jquery'), '1.0.0', true );
    wp_enqueue_script( 'script-scrolloverflow', get_template_directory_uri() . '/assets/dist/js/scrolloverflow.min.js', array('jquery'), '1.0.0', true );
    wp_enqueue_script( 'script-fullpage', get_template_directory_uri() . '/assets/dist/js/jquery.fullPage.min.js', array('jquery'), '1.0.0', true );
    wp_enqueue_script( 'script-revealator', get_template_directory_uri() . '/assets/dist/js/revealator.min.js', array('jquery'), '1.0.0', true );
    wp_enqueue_script( 'script-parallax', get_template_directory_uri() . '/assets/dist/js/parallax.js', array('jquery'), '1.0.0', true );
    wp_enqueue_script( 'script-index', get_template_directory_uri() . '/assets/dist/js/index.js', array('jquery'), '1.0.0', true );

}