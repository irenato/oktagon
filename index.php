<?php get_header(); ?>
<div id="fullpage">
    <section class="section first_screen">

        <div class="container revealator-slideleft revealator-once revealator-delay3 ">
            <div class="big_roblex revealator-slideright revealator-once revealator-delay8 revealator-duration13"></div>
            <div class="content">

                <header>
                    <div class="logo_oct">
                        <img src="http://localhost/oktagon/assets/images/logo_oct.png">
                    </div>
                    <div class="rob_button">
                        <button><span>+</span> Открыть счет</button>

                        <div class="logo_rob">
                            <img src="http://localhost/oktagon/assets/images/logo_rob.png">
                        </div>
                    </div>
                    <div class="lang">
                    </div>

                </header>
                <div class="middle">
                    <h1>
                        БУДУЩЕЕ УЖЕ наступило
                    </h1>
                    <p>
                        Новейший торговый робот <strong>ROBLEX</strong> эффективнее 99% трейдеров не упусти свой шанс
                        обойти
                        всех
                    </p>
                </div>
                <div class="bottom">
                    <div class="more_info">

                        <a data-menuanchor="secondPage" href="#secondPage">
                            <button>Подробнее <span></span></button>
                        </a>


                        <ul>
                            <li> Доступно только клиентам <strong>OCTAGON CAPITAL MARKETS</strong></li>
                        </ul>

                    </div>
                </div>


            </div>

            <div class="robot_place">
                <div class="p_relative">
                    <img class="lines_left "
                         src="http://localhost/oktagon/assets/images/lines_left.svg">
                    <img class="lines_right"
                         src="http://localhost/oktagon/assets/images/lines_right.svg">

                    <div class="circle_place">
                        <div class="circle circle_3">
                            <div class="circle circle_2">
                                <div class="circle circle_1">

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="robot revealator-slideup revealator-once revealator-delay5 revealator-duration7">
                        <img class="" src="http://localhost/oktagon/assets/images/robot.png">
                    </div>
                </div>
            </div>
        </div>

        <div class="romb  revealator-zoomin  revealator-once revealator-delay5"><img
                src="http://localhost/oktagon/assets/images/romb.png"></div>



    </section>


    <section class="section second_screen" data-anchor="secondPage">

        <div class="container">
            <div class="content">
                <div class="top">
                    <div class="left_side">
                        <h2>Средняя прибыль</h2>

                        <div class="calculating_form">
                            <div class="rang_1">
                                <label>Период</label>
                                <div class="range-slider">
                                    <span id="rs-bullet" class="rs-label">0</span>
                                    <input id="rs-range-line" class="rs-range" type="range" value="0" min="0" max="12">

                                </div>


                            </div>

                            <div class="rang_2">
                                <label>Размер Инвестиций</label>
                                <div class="range-slider">
                                    <span id="rs-bullet2" class="rs-label">0</span>
                                    <input id="rs-range-line2" class="rs-range" type="range" value="0" min="0"
                                           max="30000">
                                </div>
                            </div>

                            <div class="bottom">
                                <div class="result">
                                    <span>Доход:</span>
                                    <p>$ 28 230</p>
                                </div>

                                <button>НАЧАТЬ СЕЙЧАС</button>
                            </div>
                        </div>
                    </div>
                    <div class="right_side">
                        <div class="info_list">
                            <div class="single">
                                <div class="info_icon">
                                    <img src="http://localhost/oktagon/assets/images/s_list_icon.png">
                                    <span>4560%</span>
                                </div>

                                <p>Максимальная доходность</p>
                            </div>
                            <div class="single">
                                <div class="info_icon">
                                    <img src="http://localhost/oktagon/assets/images/s_list_icon.png">
                                    <span>4560%</span>
                                </div>

                                <p>Максимальная доходность</p>
                            </div>
                            <div class="single">
                                <div class="info_icon">
                                    <img src="http://localhost/oktagon/assets/images/s_list_icon.png">
                                    <span>4560%</span>
                                </div>

                                <p>Максимальная доходность</p>
                            </div>
                            <div class="single">
                                <div class="info_icon">
                                    <img src="http://localhost/oktagon/assets/images/s_list_icon.png">
                                    <span>4560%</span>
                                </div>

                                <p>Максимальная доходность</p>
                            </div>
                        </div>
                    </div>

                </div>


            </div>

            <div class="romb  "><img
                    src="http://localhost/oktagon/assets/images/romb_b.png"></div>
        </div>
    </section>

    <section class="section third_screen" data-anchor="thirdPage">
        <div class="container">
            <div class="content">
                <div class="top">
                    <div class="left_side">
                        <div class="title">
                            <h2>ПРИКОСНИСЬ </h2>
                            <h3>К новейшим ТЕНОЛОГИЯМ торговли на FOrex</h3>
                        </div>


                        <div class="contact_form ">
                            <form>
                                <div class="left">
                                    <input type="text" name="name" placeholder="Ваше имя">
                                    <input type="text" name="p_name" placeholder="Фмилия">
                                    <button>НАЧАТЬ СЕЙЧАС</button>
                                </div>
                                <div class="right">
                                    <input type="email" name="email" placeholder="E-mail">
                                    <input type="tel" name="tel" placeholder="7+">
                                    <ul>
                                        <li> Доступно только клиентам <strong>OCTAGON CAPITAL MARKETS</strong></li>
                                    </ul>
                                </div>
                                <div class="center"></div>
                            </form>
                        </div>
                    </div>
                    <div class="right_side">
                        <div id="scene" class="hand_place">

                            <img data-depth="0.8" src="http://localhost/oktagon/assets/images/hand.png">
                        </div>
                    </div>

                </div>


            </div>
        </div>
        <div class="romb  "><img
                src="http://localhost/oktagon/assets/images/romb_b.png"></div>
    </section>


    <section class="section fourth_screen" data-anchor="fourthPage">
        <div class="container">

            <div class="star_place">
                <div id="scene2">
                    <div class="stars" data-depth="0.4">
                        <img src="http://localhost/oktagon/assets/images/star.png">
                    </div>
                </div>
            </div>

            <div class="content">
                <div class="text_place">
                    <div  id="scene4">

                        <div class="text" data-depth="0.8">
                            <h3>Будь в тренде испытай технологии свободы</h3>
                            <h2>свободы</h2>
                            <h4>Свободы времени, жизни, любви, cвободы денег...</h4>
                        </div>

                    </div>
                </div>



            </div>

        </div>

        <div class="man_place">
            <div id="scene3" data-limit-y="0" >
                <div class="man"  data-depth="0.2"><img src="http://localhost/oktagon/assets/images/man.png"></div>
            </div>
        </div>

    </section>

    <section class="section fifth_screen" data-anchor="fifthPage">
        <div class="container">

            <div class="content">
                <div class="top">
                    <div class="left_side">
                        <h3>Получи преимущество искусственного интеллекта</h3>
                    </div>
                    <div class="right_side">
                        <div class="left">
                            <button>НАЧАТЬ СЕЙЧАС</button>
                        </div>
                        <div class="right">
                            <ul>
                                <li> Доступно только клиентам <strong>OCTAGON CAPITAL MARKETS</strong></li>
                            </ul>
                        </div>

                    </div>
                </div>
                <div class="middle1">
                    <div class="left_side ">

                        <div class="logo_oct">
                            <img src="http://localhost/oktagon/assets/images/logo_oct.png">
                        </div>

                    </div>
                    <div class="right_side">
                        <div class="left">
                            <ul>
                                <li><a href="#">AML Policy</a></li>
                                <li><a href="#">KYC Policy</a></li>
                                <li><a href="#">Risk disclaimer</a></li>
                            </ul>
                            <div class="phone">
                                <span>Phone</span>
                                <a href="tel: +44 208 0895898">+44 208 0895898</a>
                            </div>
                        </div>
                        <div class="right">
                            <ul>
                                <li><a href="#">AML Policy</a></li>
                                <li><a href="#">KYC Policy</a></li>
                                <li><a href="#">Risk disclaimer</a></li>
                            </ul>
                            <div class="mail">
                                <span>E-mail</span>
                                <a href="mailto:support@octagonfx.com">support@octagonfx.com</a>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="middle2">
                    <div class="left_side ">
                        <div class="logo_rob">
                            <img src="http://localhost/oktagon/assets/images/roblex_footer.png">
                        </div>
                    </div>
                    <div class="right_side">
                        <p>
                            THE WEBSITE OCTAGONFX.COM IS OWNED AND OPERATED BY OCM LLC, REGISTERED AND REGULATED IN
                            Saint Vincent and the Grenadines WITH ADDRESS IN First Floor, First St Vincent Bank Ltd
                            Building, James Street, Kingstown, Saint Vincent and the Grenadines AND SERVES AS A PAYMENT
                            AGENT AND CURRENCY EXCHANGE AGENT BASED ON LEGAL OPINIONS.
                        </p>
                    </div>
                </div>
                <div class="bottom">
                    <div class="left_side ">
                        <span>© 2019 OCM LLC ALL RIGHTS RESERVED</span>
                    </div>
                    <div class="right_side">
                        <div class="partners">
                            <div>
                                <img src="/oktagon/assets/images/bitcoin.png">
                            </div>
                            <div>
                                <img src="/oktagon/assets/images/paypal.png">
                            </div>
                            <div>
                                <img src="/oktagon/assets/images/mastercard.png">
                            </div>
                            <div>
                                <img src="/oktagon/assets/images/mir.png">
                            </div>
                            <div>
                                <img src="/oktagon/assets/images/visa.png">
                            </div>
                        </div>
                    </div>
                </div>


            </div>

        </div>


    </section>
</div>


<?php get_footer(); ?>